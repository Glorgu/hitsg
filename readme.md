**HitSG Readme**
--

HitSG is a scenario generator for the reboot Hitman series. Based off of the Giant Bomb Hitsmas videos from years back, it will give you specific requirements for how to kill each target in the level, and a additional wildcard modifier you must follow. There is support for each main story mission in **Hitman (2016)**, **Hitman II**, and  **Hitman III**.

---
Running HitSG
-

HitSG runs in the terminal. In order to access it in the terminal, you'll need to have the terminal pointed to the directory where hitsg.py is located. You will also need python to be able to run the script.

If you are unfamiliar with navigating directories using the terminal, an easy way to do this is to navigate to the folder using Windows Explorer, click into the address bar, type "cmd", and press enter.

HitSG also requires entering arguments to work correctly. These are additional variables you'll type into the terminal after the executable name. The following arguments are used:

1. Location: (REQUIRED) This argument determines which location HitSG will be pulling scenario information for. This argument needs to match the name of the folder associated with the location in the "Location" folder. For example, New York must be entered as "NY".

2. Wildcards: (OPTIONAL) This argument determines how many wildcard modifiers you will be given at the end of the mission prompt. You can leave this blank or put in as large a number as you want as long as it does not exceed how many wildcards are available.

Below are some example arguments:

```
python hitsg.py Paris 1
```

Output a mission prompt for Paris with 1 wildcard modifier.

```
python hitsg.py NY 3
```

Output a mission prompt for New York with 3 wildcard modifiers.

```
python hitsg.py Mendoza
```
Output a mission prompt for Mendoza with no wildcard modifiers.

---
Modifying HitSG
--
You can edit any of the target, weapon, disguise, or wildcard lists in their respective folders. Just be sure to keep each option on it's own separate line.


---

About Colorado and Berlin
--

In practice, I didn't find running prompts for all the targets in Colorado or Berlin to be much fun, so I instead removed all target, weapon, and disguise information, and have been running those missions just trying to complete as many wildcard modifiers as possible. I have found the sweet spot to be outputting 6 modifiers and picking 5 of them to run. Feel free to add the targets, weapons, and disguises yourself if you want to.

---

Target-specific Weapon Pools
--

You can modify the weapon-pools avaible per target in a location by creating target-specific files for each target in that locaiton. The file name needs to be formatted like below:
```
weapon_TargetName.txt
```
The target names must match those of each target name in the target.txt folder, but without spaces.
 
 This is done by default for Hokkaido, as there are weapons that cannot be used to kill Erich Soders. Use that as a reference if you want to recreate this for another location.

