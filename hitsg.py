import sys, random, time, os

def delay_print(s, t):
    for c in s:
        sys.stdout.write(c)
        sys.stdout.flush()
        time.sleep(t)
    sys.stdout.write("\n")
    sys.stdout.flush()
# sys.argv[1]: Location name (see directories for names).
# sys.argv[2]: Amount of wildcard modifiers to use.

targets = open('Location/' + sys.argv[1] + '/target.txt').read().splitlines()
disguises = open('Location/' + sys.argv[1] + '/disguise.txt').read().splitlines()


# Check if target specific weapon files exist for location.
weapons = list()
multi_weapons = False
for fn in os.listdir('Location/' + sys.argv[1]):
    if fn.__contains__('weapon_'):
        multi_weapons = True
if multi_weapons == True:
    weapon_files = [fn for fn in os.listdir('Location/' + sys.argv[1]) if fn.__contains__('weapon_')]
    for fn in weapon_files:
        # [target_name, [weapon_list]]
        weapons.extend([[fn[fn.find('_')+1 : fn.find('.')],open('Location/' + sys.argv[1] + '/' + fn).read().splitlines()]])
else:
    weapons.extend(open('Location/' + sys.argv[1] + '/weapon.txt').read().splitlines())

# set time_wait for delay between prompts.
time_wait = 1.0
# set time_type for speed of written output. Increase for dramatic effect!
time_type = 0.025

disguise = random.sample(disguises, len(targets))
weapon = list()

if multi_weapons == True:
    for wt in weapons:
        
        for t in targets:
            if(t.replace(" ", "") == wt[0]):
                weapon.extend(random.sample(wt[1], 1))
else:
    weapon = random.sample(weapons, len(targets))

delay_print("\nHitman Scenario Generator : " + sys.argv[1] + "\n", time_type)
time.sleep(time_wait)

for t in range(len(targets)):
    delay_print("You must kill: " + targets[t], time_type)
    time.sleep(time_wait)
    delay_print("Disguised as: " + disguise[t], time_type)
    time.sleep(time_wait)
    delay_print("Using: " + weapon[t], time_type)
    print("")
    time.sleep(time_wait * 1.5)

if len(sys.argv) > 2:
    wildcards = open('Location/' + '_Baseline' + '/wildcard.txt').read().splitlines()
    wildcards.extend(open('Location/' + sys.argv[1] + '/wildcard.txt').read().splitlines())
    wildcard = random.sample(wildcards, int(sys.argv[2]))

    delay_print("\nWildcard Modifiers:\n", time_type)
    for w in range(int(sys.argv[2])):
        time.sleep(time_wait)
        delay_print(wildcard[w], time_type)